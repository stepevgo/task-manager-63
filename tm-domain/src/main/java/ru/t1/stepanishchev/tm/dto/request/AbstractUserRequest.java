package ru.t1.stepanishchev.tm.dto.request;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String token;

    public AbstractUserRequest(@Nullable final String token) {
        this.token = token;
    }

}