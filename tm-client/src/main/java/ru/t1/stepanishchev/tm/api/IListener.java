package ru.t1.stepanishchev.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    String getArgument();

    @Nullable
    String getName();

    @Nullable
    String getDescription();

    void handler(ConsoleEvent consoleEvent);

}