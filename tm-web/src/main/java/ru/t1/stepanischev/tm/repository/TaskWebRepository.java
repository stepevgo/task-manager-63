package ru.t1.stepanischev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanischev.tm.model.TaskWeb;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskWebRepository {

    @Nullable
    private static final TaskWebRepository INSTANCE = new TaskWebRepository();

    public static TaskWebRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, TaskWeb> tasks = new LinkedHashMap<>();

    {
        add(new TaskWeb("First_Task"));
        add(new TaskWeb("Second_Task"));
        add(new TaskWeb("Third_Task"));
    }

    public void add(@NotNull final TaskWeb task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        add(new TaskWeb("New Task " + System.currentTimeMillis()));
    }

    @NotNull
    public Collection<TaskWeb> findAll() {
        return tasks.values();
    }

    @Nullable
    public TaskWeb findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void save(@NotNull final TaskWeb task) {
        tasks.put(task.getId(), task);
    }

}