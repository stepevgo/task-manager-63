package ru.t1.stepanischev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanischev.tm.model.ProjectWeb;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectWebRepository {

    @NotNull
    private static final ProjectWebRepository INSTANCE = new ProjectWebRepository();

    public static ProjectWebRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, ProjectWeb> projects = new LinkedHashMap<>();

    {
        add(new ProjectWeb("First_Project"));
        add(new ProjectWeb("Second_Project"));
        add(new ProjectWeb("Third_Project"));
    }

    public void add(@NotNull final ProjectWeb project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new ProjectWeb("New Project " + System.currentTimeMillis()));
    }

    @NotNull
    public Collection<ProjectWeb> findAll() {
        return projects.values();
    }

    @Nullable
    public ProjectWeb findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    public void save(@NotNull final ProjectWeb project) {
        projects.put(project.getId(), project);
    }

}