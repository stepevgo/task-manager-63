package ru.t1.stepanischev.tm.servlet;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanischev.tm.enumerated.Status;
import ru.t1.stepanischev.tm.model.TaskWeb;
import ru.t1.stepanischev.tm.repository.ProjectWebRepository;
import ru.t1.stepanischev.tm.repository.TaskWebRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(@NotNull HttpServletRequest req, @NotNull HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @Nullable final TaskWeb task = TaskWebRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectWebRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String projectId = req.getParameter("projectId");
        @NotNull final String name = req.getParameter("name");
        @Nullable final String description = req.getParameter("description");
        @Nullable final String statusValue = req.getParameter("status");
        @Nullable final Status status = Status.valueOf(statusValue);
        @NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        @NotNull final String dateStartValue = req.getParameter("dateStart");
        @NotNull final String dateFinishValue = req.getParameter("dateFinish");

        @NotNull final TaskWeb task = new TaskWeb();
        task.setId(id);
        task.setProjectId(projectId);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);

        if (!dateStartValue.isEmpty()) task.setDateStart(simpleDateFormat.parse(dateStartValue));
        else task.setDateStart(null);
        if (!dateFinishValue.isEmpty()) task.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else task.setDateFinish(null);

        TaskWebRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}