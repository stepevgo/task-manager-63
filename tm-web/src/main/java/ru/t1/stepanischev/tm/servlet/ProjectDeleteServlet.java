package ru.t1.stepanischev.tm.servlet;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanischev.tm.repository.ProjectWebRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/delete/*")
public class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(@NotNull HttpServletRequest req, @NotNull HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        ProjectWebRepository.getInstance().removeById(id);
        resp.sendRedirect("/projects");
    }

}